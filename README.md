# Formation_pyrrha_novembre_2020

Ce projet contient les documents et supports du cours sur l'outil de post-correction Pyrrha du 9 novembre 2020 dans le cadre du master HN de l'ENSSIB. 

## Fonctionnement

Les données sont extraites de `corpus/corpus_tsv` dans `out`. Le texte traité est *Andromaque* dans son édition de 1676 (édition complète des oeuvres de Racine), est également divisé entre les participant.es à la formation. Vous trouverez le texte à corriger dans `out/Racine1676_Oeuvres1_bpt6k9905809_corrected_numb_align_l`.


## Crédits


Les fichiers sources en .tsv sont tirés du dépôt git de Simon Gabay [e-ditiones/PARALLEL17](https://github.com/e-ditiones/PARALLEL17), source de l'article suivant: 
+ Gabay, Simon, et Loïc Barrault. « Traduction automatique pour la normalisation du français du XVII e siècle ». In TALN 2020. 27ème Conférence sur le Traitement Automatique des Langues Naturelles. Nancy, France: ATALA, 2020. https://hal.archives-ouvertes.fr/hal-02596669.

Je l'en remercie !
