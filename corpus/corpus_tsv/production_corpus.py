import os
import glob
import sys

path = "corpus_tsv/Racine1676_Oeuvres1_bpt6k9905809_corrected_numb_align_l.tsv"
liste_eleves  = ['Ariane', 'Sylvain', 'Emma', 'Mathilde', 'Monique', 'Emelyne', 'Alienor', 'Gaelle', 'Eric', 'Melanie', 'Julien', 'Orline', 'Camille', 'Theau', 'Cahal', 'Hasna', 'Leonhard']

for fichier in glob.glob(path):
	author_date = fichier.split("/")[-1].split("_")[0]
	title = fichier.split("/")[-1].split("_")[1]
	with open(fichier, "r") as f:
		liste_fichier = [line.split('\t')[0] for line in f.readlines()]
		lignes_par_eleve = round(len(liste_fichier) / len(liste_eleves)) - 1 
		mon_dict = {i:  "\n".join(liste_fichier[liste_eleves.index(i)*lignes_par_eleve:liste_eleves.index(i)*lignes_par_eleve + 100]) for i in liste_eleves}
		for eleve, section in mon_dict.items(): 
			with open(f"out/{eleve}.txt", "w") as out_file:
				out_file.write(section)

print("Travail terminé !")

