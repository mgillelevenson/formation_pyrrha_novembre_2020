Ah, Madame! Les Grecs, ſi j’en çroy leurs allarmes,
Vous donneront bien-toſt d’autres ſujets de larmes.
Et quelle eſt cette peur dont leur Cœur eſt frappé,
Seigneur? Quelque Troyen vous eſt-il échappé?
Leur haine pour Hector n’eſt pas encore éteinte.
Ils redoutent ſon Fils.
Digne Objet de leur crainte!
Un Enfant malheureux, qui ne ſçait pas encor
Que Pyrrhus est ſon Maître, & qu'il eſt Fils d'Hector.
Tel qu’il eſt, tous les Grecs demandent qu’il periſſe.
Le Fils d’Agamemnon vient haſter ſon ſupplice.
Et vous prononcerez un Arreſt ſi cruel?
Eſt-ce mon intereſt qui le rend criminel?
Helas! on ne craint point qu'il vange un jour ſon Pere.
On craint qu’il n’eſſuyaſt les larmes de ſa Mere.
Il m’auroit tenu lieu d'un Pere, & d'un Epoux.
Mais il me faut tout perdre, & toûjours par vos coups.
Madame, mes refus ont prévenu vos larmes.
Tous les Grecs m’ont déja menacé de leurs Armes,
Mais deuſſent-ils encore, en repaſſant les Eaux,
Demander voſtre Fils, avec mille Vaiſſeaux:
Couſtaſt-il tout le ſang qu’Helene a fait répandre,
Deuſſay-je apres dix ans voir mon Palais en cendre,
Je ne balance point, je vole à ſon ſecours,
Je defendray ſa vie aux deſpens de mes jours.
Mais parmy ces perils, où je cours pour vous plaire,
Me refuſerez-vous un regard moins ſevere?
Haï de tous les Grecs, preſſé de tous coſtez,
Me faudra-t’il combattre encor vos cruautez?
Je vous offre mon bras. Puis-je eſperer encore
Que vous accepterez un cœur qui vous adore?
En combattant pour vous, me ſera-t'il permis
De ne vous point conter parmy mes Ennemis?
Seigneur, que faites-vous, & que dira la Grece?
Faut-il qu’un ſi grand cœur montre tant de foibleſſe?
Voulez-vous qu'un deſſein fi beau, ſi genereux,
Paſſe pour le tranſport d’un Eſprit amoureux?
Captive, toujours triſte, importune à moy-meſme,
Pouvez-vous ſouhaiter qu’Andromaque vous aime?
Quels charmes ont pour vous des yeux infortunez,
Qu'à des pleurs éternels vous avez condannez?
Non, non, d’un Ennemy reſpecter la Miſere,
Sauver des Mal-heureux, rendre un Fils à ſa Mere,
De cent Peuples pour luy combattre la rigueur,
Sans me faire payer ſon ſalut de mon Cœur,
Malgré moy, s’il le faut, luy donner un azile,
Seigneur, voilà des ſoins dignes du Fils d’Achille.
Hé quoy? Voſtre courroux n’a-t’il pas eû ſon cours?
Peut-on haïr ſans ceſſe? Et punit-on toûjours?
J’ay fait des Mal-heureux, ſans doute, & la Phrygie
Cent fois de voſtre ſang a vû ma main rougie.
Mais que vos yeux ſur moy ſe ſont bien exercez!
Qu'ils m’ont vendu bien cher les pleurs qu’ils ont verſez!
De combien de remords m’ont-ils rendu la proye?
Je ſouffre tous les maux que j’ay faits devant Troye.
Vaincu, chargé de fers, de regrets conſumé,
Brûlé de plus de feux que je n’en allumé,
Tant de ſoins, tant de pleurs, tant d’ardeurs inquiétes...
Hela! fus-je jamais ſi cruel que vous l’eſtes?
Mais enfin, tour à tour, c’eft allez nous punir.
Nos ennemis communs devroient nous reünir.
Madame, dites-moy ſeulement que j’eſpere,
Je vous rends voſtre Fils, & je luy ſers de Pere.
Je I'inſtruiray moy-meſme à vanger les Troyens.
J’iray punir les Grecs de vos maux & des miens,
Animé d'un regard, je puis tout entreprendre.
Voſtre Ilion encor peut ſortir de ſa cendre.
Je puis, en moins de temps que les Grecs ne l'ont pris,
Dans ſes Murs relevez couronner voſtre Fils.
Seigneur, tant de grandeurs ne nous touchent plus guére,
Je les luy promettois tant qu’a vécu ſon Pere.
Non, vous n’eſperez plus de nous revoir encor,
Sacrez Murs, que n’a pu conſerver mon Hector.
A de moindres faveurs des mal-heureux prétendent,
Seigneur. C’eſt un Exil que mes pleurs vous demandent.
Souffrez que loin des Grecs, & meſme loin de vous,
J’aille cacher mon Fils, & pleurer mon Epoux.
Voſtre amour contre nous allume trop de haine.
Retournez, retournez à la Fille d’Helene.
Et le puis-je, Madame? Ah, que vous me geſnez!
Comment luy rendre un Cœur que vous me retenez?
Je ſçay que de mes vœux on luy promit l’empire.
je ſçay que pour regner elle vint dans l’Epire.
Le Sort vous y voulut l’une & l’autre amener,
Vous pour porter des fers, Elle pour en donner.
Cependant ay-je pris quelque ſoin de luy plaire?
Et ne diroit-on pas, en voyant au contraire,
Vos charmes tout -puiſſans, & les ſiens dédaignez,
Quelle eſt icy Captive, & que vous y regnez?
Ah! qu’un ſeul des ſoûpirs, que mon Cœur vous envoye,