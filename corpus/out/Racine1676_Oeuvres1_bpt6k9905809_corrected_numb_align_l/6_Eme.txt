Prenons quelque plaiſir à leur eſtre importune.
Ou le forçant de rompre un nœud ſi ſolemnel,
Aux yeux de tous les Grecs rendons-le criminel.
J’ay déja ſur le Fils attiré leur colere.
Je veux qu’on vienne encor luy demander la Mere.
Rendons luy les tourmens qu'elle me fait ſouffrir.
Quelle le perde, ou bien qu’il la faſſe périr.
Vous penſez que des yeux toûjours ouverts aux larmes,
Se plaiſent à troubler le pouvoir de vos charmes?
Et qu’un Cœur accablé de tant de déplaiſirs,
De ſon Perſecuteur ait brigué les ſoûpirs?
Voyez ſi ſa douleur en paroiſt ſoulagée.
Pourquoy donc les chagrins où ſon ame eſt plongée?
Contre un Amant qui plaiſt pourquoy tant de fierté?
Helas! pour mon malheur je l’ay trop écouté.
Je n’ay point du ſilence affecté myſtere.
Je croyois ſans péril pouvoir eſtre ſincere.
Et ſans armer mes yeux d’un moment de rigueur,
Je n’ay pour luy parler, conſulté que mon cœur.
Et qui ne ſe ſeroit comme moy declarée,
Sur la foy d’une amour ſi ſaintement jurée?
Me voyoit-il de l’œil qu’il me voit aujourd’huy?
Tu t’en ſouviens encor, tout conſpiroit pour luy.
Ma famille vangée, & les Grecs dans la joye,
Nos Vaiſſeaux tout chargez des dépoüilles de Troye,
Les Exploits de ſon Pere, effacez par les ſiens,
Ses feux que je croyois plus ardens que les miens,
Mon Cœur, toy-meſme enfin de ſa gloire ébloüie,
Avant qu’il me trahiſt, vous m’avez tous trahie.
Mais c’en eſt trop, Cleone, & quel que ſoit Pyrrhus,
Hermione eſt ſenſible, Oreſte a des vertus.
Il ſçait aimer du moins, & meſme ſans qu’on l’aime;
Et peut-eſtre il ſçaura ſe faire aimer luy-meſme.
Allons. Qu'il vienne enfin.
Madame, le voicy.
Ah! je ne croyois pas qu’il fuſt ſi prés d’icy.
LE croiray-je, Seigneur, qu’un reſte de tendreſſe
Vous faſſe icy chercher une triſte Princeſſe?
Ou ne dois-je imputer qu’à voſtre ſeul devoir,
L’heureux empreſſement qui vous porte à me voir?
Tel eſt de mon amour l’aveuglement funeſte.
Vous le ſçavez, Madame, & le deſtin d’Oreſte
Eſt de venir ſans ceſſe adorer vos attraits,
Et de jurer toûjours qu’il n’y viendra jamais.
Je ſçay que vos regards vont rouvrir mes bleſſures,
Que tous mes pas vers vous ſont autant de parjures.
Je le ſçay, j’en rougis. Mais j’atteſte les Dieux,
Témoins de la fureur de mes derniers adieux,
Que j’ay couru par tout, où ma perte certaine
Dégageoit mes ſermens, & finiſſoit ma peine.
J’ay mandié la Mort, chez des Peuples cruels
Qui n’apaiſoient leurs Dieux que du ſang des Mortels:
Ils m’ont fermé leur Temple, & ces Peuples barbares
De mon ſang prodigué ſont devenus avares.
Enfin je viens à vous, & je me voy reduit
A chercher dans vos yeux une mort, qui me fuit.
Mon deſeſpoir n’attend que leur indifference,
Ils n’ont qu’à m’interdire un reſte d’eſperance.
Ils n’ont, pour avancer cette mort où je cours,
Qu'à me dire une fois ce qu’ils m’ont dit toûjours,
Voila depuis un an le ſeul ſoin qui m’anime.
Madame, c’eſt à vous de prendre une Victime,
Que les Scythes auroient dérobée à vos coups,
Si j’en avois trouvé d’auſſi cruels que Vous.
Quittez, Seigneur, quittez ce funeſte langage.
A des ſoins plus preſſans la Grece vous engage.
Que parlez-vous du Scythe, & de mes cruautez?
Songez à tous ces Rois que vous repreſentez.
Faut-il que d’un tranſport leur Vangeance dépende?
Eſt-ce le ſang d’Oreſte enfin qu’on vous demande?
Dégagez-vous des ſoins dont vous eſtes chargé.
Les refus de Pyrrhus m’ont aſſez dégagé,
Madame, il me renvoye, & quelque autre Puiſſance
Lui fait du Fils d’Hector embraſſer la défenſe.
L’infidelle!
Ainſi donc tout preſt à le quitter,
Sur mon propre deſtin je viens vous conſulter.
Déja meſme je crois entendre la reponſe
Qu'en ſecret contre moy voſtre haine prononce.
Hé quoy? toûjours injuſte en vos triſtes diſcours,
De mon inimitié vous plaindrez-vous toûjours?
Quelle eſt cette rigueur tant de fois alleguée?
J’ay paſſé dans l’Epire où j’eſtois releguée?
Mon Pere l’ordonnoit. Mais qui ſçait ſi depuis,
Je n’ay point en ſecret partagé vos ennuis?
Penſez-vous avoir ſeul éprouvé des allarmes?
Que l’Epire jamais n’ait veû couler mes larmes?
Enfin, qui vous a dit, que malgré mon devoir,
Je n’ay pas quelquefois ſouhaitté de vous voir?
Souhaitté de me voir? Ah divine Princeſſe....