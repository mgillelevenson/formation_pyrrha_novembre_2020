import os
import glob
import shutil

liste_des_eleves  = ['Ar', 'Sy', 'Em', 'Ma', 'Mon', 'Eme', 'Al', 'Ga', 'Er', 'Me', 'Ju', 'Or', 'Cam', 'Th', 'Cah', 'Ha', 'Le', 'Mar', 'Mat']
chemin = "corpus_tsv/Racine1676_Oeuvres1_bpt6k9905809_corrected_numb_align_l.tsv" # le chemin vers les fichiers à traiter


def main(liste_eleves, path):
	liste_eleves.sort()
	liste_eleves = [f'{liste_eleves.index(i)+1}_{i}' for i in liste_eleves]

	for fichier in glob.glob(path):
		nom_fichier = fichier.split("/")[-1].split(".")[0]
		try:
			os.mkdir(f'out/{nom_fichier}')
		except:
			shutil.rmtree(f'out/{nom_fichier}')
			os.mkdir(f'out/{nom_fichier}')
		
		with open(fichier, "r") as f:
			liste_fichier = [line.split('\t')[0] for line in f.readlines()] # chaque ligne contient le texte diplomatique et le texte normalisé. On 
			# ne garde que le texte diplomatique
			# On arrondit à l'entier le plus proche le nombre de lignes à traiter par élève
			lignes_par_eleve = round(len(liste_fichier) / len(liste_eleves))

			# On crée un dictionnaire de la forme {"élève": "texte"}
			mon_dict = {i:  "\n".join(liste_fichier[liste_eleves.index(i)*lignes_par_eleve:liste_eleves.index(i)*lignes_par_eleve + lignes_par_eleve]) for i 																			in liste_eleves[0:-1]} # attention avec le *list slicing*: la borne de fin est exclusive.
			# Le dernier segment de texte est probablement un peu différent du reste
			# on le traite donc à part pour éviter les exceptions de type List index out of range
			mon_dict[liste_eleves[-1]] = "\n".join(liste_fichier[liste_eleves.index(liste_eleves[-1])*lignes_par_eleve:])

			# On écrit les fichiers à partir du dictionnaire. 
			for eleve, section in mon_dict.items(): 
				with open(f"out/{nom_fichier}/{eleve}.txt", "w") as out_file:
					out_file.write(section)
		
		with open(f"out/{nom_fichier}/info.txt", "w") as info_file:
			info_file.write(f"Nombre de lignes: {len(liste_fichier)}\nNombre de lignes par élèves: {lignes_par_eleve}")

	print("Travail terminé !")


if __name__ == "__main__":
	main(liste_des_eleves, chemin)

